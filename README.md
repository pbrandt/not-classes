# not-classes

[`not-classes`](https://npmjs.org/package/not-classes) is an easy to use type system that is not es6 classes. Built on top of [`duck-type`](https://npmjs.org/package/duck-type).

`npm install not-classes` or `yarn add not-classes`

**Example**

```javascript
const type = require('not-classes')

// Type definitions
const Pet = type({
  name: String,
  owner: type('Person'),
})

const Person = type({
  name: String,
  pets: [Pet],
  callPets() {
    this.pets.forEach(pet => {
      console.log('Here, ' + pet.name + '!')
    })
  }
})

// Type instances
var peter = Person({
  name: 'Peter',
  pets: []
})

var fido = Pet({
  name: 'Fido',
  owner: peter
})


peter.pets.push(fido)
peter.callPets()

```

check the tests and examples for more features, but this should get you started
