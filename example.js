const type = require('./index')

// Type definitions
const Pet = type({
  name: String,
  owner: type('Person'),
})

const Person = type({
  name: String,
  pets: [Pet],
  callPets() {
    this.pets.forEach(pet => {
      console.log('Here, ' + pet.name + '!')
    })
  }
})

// Type instances
var peter = Person({
  name: 'Peter',
  pets: []
})

var fido = Pet({
  name: 'Fido',
  owner: peter
})


peter.pets.push(fido)
peter.callPets()


