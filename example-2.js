var type = require('./index')

//
// Type definition
//
var Person = type('Person', {
  name: String,
  age: Number,
  greet: function () {
    console.log("Hi, I'm " + this.name)
  },
  favorite_person: type('Person')
})

//
// things that are of that type
//
var gus = Person({
  name: 'Gus',
  age: 77,
  favorite_person: null
})

var peter = Person({
  name: 'Peter',
  age: 75,
  favorite_person: gus
})

// you can inspect types with the nonenumerable property __type
console.log('var peter has type', peter.__type) // Person

// methods just work the way you expect
peter.greet() // Hi, I'm Peter

//
// Do argument checking
//
function main(dude) {
  Person(dude) // this checks that dude is a Person

  console.log(`Let's talk about ${dude.name}`)
  if (dude.favorite_person) {
    console.log(`${dude.name}'s favorite person is ${dude.favorite_person.name}`)
  } else {
    console.log(`${dude.name} doesn't have a favorite person and lives a sad, lonely life`)
  }
}

// try out that function with a Person
main(peter)

// expect the function to throw an error if it's not a person
try {
  main(12)
} catch (e) {
  console.log('trying to run main with an argument that wanst a person failed')
  console.log(e.message)
}
