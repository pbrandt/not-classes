var type = require('./index')
var assert = require('assert')

var test_cases = [

  {
    test_description: 'simple types',

    type_name: 'Thing',

    type_definition: {
      name: String
    },

    should_pass: [
      { name: 'LEGO' },
      { name: 'bike', age: 12 }
    ],

    should_fail: [
      null,
      { NAME: 'LEGO' },
      { name: 12, age: 'bike' },
      { name: { name: 'LEGO' } }
    ]
  },

  {
    test_description: 'types with default values',

    type_name: 'ThingWithDefault',

    type_definition: {
      name: 'doge'
    },

    should_pass: [
      {},
      { name: 'not a dogge' },
      { name: 'doge' }
    ],

    should_fail: [
      { name: 12 }
    ]
  },

  {
    test_description: 'nested objects',

    type_name: 'nestedobject',

    type_definition: {
      obj: {
        a: {
          b: String
        }
      }
    },

    should_pass: [
      { obj: { a: { b: 'wow' }}},
      { obj: { a: { b: 'wow', c: 'random'}}}
    ],

    should_fail: [
      { obj: { a: { b: 12 }}},
      { o: { a: { b: 'wow' }}}
    ]
  },

  {
    test_description: 'nested object with default values',

    type_name: 'NestWIthDefault',

    type_definition: {
      obj: {
        a: {
          b: 'nice'
        }
      }
    },

    should_pass: [
      { obj: { a: { b: 'wow' }}},
      { o: { a: { b: 'wow' }}}, // this passing case is why you should never use default values in nested objects
      {}
    ],

    should_fail: [
      { obj: { a: { b: 12 }}}
      
    ]
  },

  {
    test_description: 'nested empty objects',

    type_name: 'EvilType',

    type_definition: {
      name: String,
      meta: {}
    },

    should_pass: [
      { name: 'me' },
      { name: 'doge', meta: { woof: 'yes' }}
    ],

    should_fail: [
      { name: 'q', meta: 12 }
    ]
  },

  {
    test_description: 'array types',

    type_name: 'ArrayThing',

    type_definition: {
      items: []
    },

    should_pass: [
      {},
      { item: [] },
      { item: [1, 2, 'wow', {} ]}
    ],

    should_fail: [
      { items: {} },
      { items: {a: 12} }
    ]
  },

  {
    test_description: 'array types with type',

    type_name: 'ArrayThing2',

    type_definition: {
      items: [String]
    },

    should_pass: [
      {},
      { item: [] },
      { item: ['wow', 'nice']}
    ],

    should_fail: [
      { items: {} },
      { items: {a: 12} },
      { items: [1, 2] }
    ]
  },

  {
    test_description: 'array types with objects',

    type_name: 'ArrayThing3',

    type_definition: {
      items: [{
        name: String
      }]
    },

    should_pass: [
      {},
      { item: [] },
      { item: [{ name: 'me' }] }
    ],

    should_fail: [
      { items: {} },
      { items: {a: 12} },
      { items: [1, 2] },
      { items: [{ name: 1 }] }
    ]
  }
]

describe('schema testing', () => {
  test_cases.map(t => {
    it(t.test_description, () => {

      var Type = type(t.type_name, t.type_definition)
      assert(typeof Type === 'function')

      t.should_pass.map(p => {
        var ok = Type(p)
      })

      t.should_fail.map(f => {
        assert.throws(() => {
          var bad = Type(f)
        }, `did not throw for ${JSON.stringify(f)}`)
      })

    })
  })
})

describe('behavior testing', () => {
  it('should work for simple types', () => {
    var Thing = type({
      name: String
    })
    assert(typeof Thing === 'function')

    var thing = Thing({
      name: 'LEGO'
    })
    assert(thing.name === 'LEGO')

    assert.throws(x => {
      var badthing = Thing({
        farts: 10000000
      })
    })
  })

  it('should work for named types', () => {
    var Thing2 = type('Thing2', {
      name: String
    })
    assert(typeof Thing2 === 'function')

    var thing = Thing2({
      name: 'LEGO'
    })
    assert(thing.name === 'LEGO')
  })

  it('should allow default values', () => {
    var Wow = type({
      str: 'wooooooow'
    })
    assert(typeof Wow === 'function')

    var wow = Wow({})
    assert(wow.str === 'wooooooow')
  })

  it('should handle nested types', () => {
    var A = type({
      q: String
    })
    var B = type('B', {
      r: Number
    })
    var C = type({
      a: A,
      b: B,
    })
    assert(typeof C === 'function')

    var a = A({ q: 'a' })
    var b = B({ r: 12 })
    var c = C({ a: a, b: b})
    assert(c.a === a)
    assert(c.b === b)
  })

  it('should handle self references', () => {
    var Me = type('Me', {
      name: String,
      me: type('Me')
    })

    var me = Me({
      name: 'Peter',
      me: null
    })
    me.me = me
    assert(me.me.me.me.me.name === 'Peter')
  })

  it.only('should handle circular references', () => {
    var First = type('First', {
      second: type('Second'),
      name: String
    })

    var Second = type('Second', {
      first: First
    })

    console.log('typeof firs', typeof First)
    assert(typeof First === 'function')
    assert(typeof Second === 'function')
    
    var f = First({
      second: null,
      name: 'f'
    })

    var s = Second({
      first: f
    })

    assert(s.first.name === 'f')

    assert.throws(function() {
      var bad_s = Second({
        first: 12
      })
    })
  })

  it('should handle object properties', () => {
    var O = type('O', {
      obj: {
        a: String,
        b: {
          c: String
        }
      }
    })

    assert(typeof O === 'function')

    var o = O({
      obj: {
        a: 'neat',
        b: {
          c: 'socks, cat'
        }
      }
    })

    assert(o.obj.b.c === 'socks, cat')
  })

  it('should handle array properties', () => {
    var List = type({
      name: String,
      items: [String]
    })

    assert(typeof List === 'function')

    var l = List({
      name: 'stuff',
      items: ['a', 'b']
    })

    assert(l.items[0] === 'a')
  })

  it('should support structured arrays', () => {
    var S = type({
      items: [{
        a: String
      }]
    })

    assert(typeof S === 'function')

    var s = S({
      items: [{
        a: 'wow'
      }]
    })

    assert(s.items[0].a === 'wow')

    assert.throws(() => {
      var s2 = S({
        items: [12]
      })
    })
  })

  it('should allow method definitions', () => {

    var Persson = type({
      name: String,
      hi: function () {
        return this.name
      }
    })
    assert(typeof Persson === 'function')

    var me = Persson({
      name: 'dufus'
    })
    assert(me.hi() === 'dufus')

    assert.throws(() => {
      var bad = Person({
        name: 'wow',
        hi: 12
      })
    })
  })
})
