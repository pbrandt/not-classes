var schema = require('duck-type').create()
var _ = require('lodash')
var customTypeCounter = 0
var allTypes = {}

function Type (name, def) {
  if (!(this instanceof Type)) {
    return new Type(name, def)
  }

  if (typeof name === 'string') {
    // make sure to capitalize name because duck-type needs it to be capitalized for some reason
    name = name[0].toUpperCase() + name.slice(1)
  }
  console.log('name', name, 'def', def)

  if (typeof name !== 'string') {
    if (Object(name) !== name) {
      throw new Error('Type error, expected format is: var MyType = type("MyType", { bar: String, foo: Number })') 
    }
    def = name
    name = 'CustomType' + (customTypeCounter++)
  } else if (typeof name === 'string' && typeof def === 'undefined') {
    
    // User is trying to access a type, maybe to create an alias or for embedding in another object
    if (!allTypes[name]) {
      // allow circular or recursive type definitions
      defineProp(this, '__temp', true)
      defineProp(this, '__type', name)
      allTypes[name] = this
      console.log('alltypes', name, allTypes[name])
    }
    return this
  }

  if (allTypes[name] && !allTypes[name].__temp) {
    console.log(allTypes[name])
    throw new Error('Type error: ' + name + ' is alreay defined')
  }

  // fist cache the default functions
  var defaults = []



  function getDefaults (currentPath, def) {
    if (Object(def) !== def) {
      return
    }

    Object.keys(def).map(k => {
      var thisPath = [currentPath, k].filter(Boolean).join('.')
      if (isDefinedFunction(def[k])) {
        // k: function () {}
        console.log(k, 'is function {}')
        defaults.push({
          path: thisPath,
          fn: def[k]
        })
        delete def[k]
      } else if (Object(def[k]) === def[k]) {
        if (Object.keys(def[k]).length > 0) {
          if (def[k] instanceof Array) {
            // k: [String]
            defaults.push({
              path: thisPath,
              v: []
            })
          } else {
            // k: { a: String, b: Number}
            getDefaults(thisPath, def[k])
          }
        } else if (typeof def[k] !== 'function') {
          // k: {}, k: []
          defaults.push({
            path: thisPath,
            v: def[k]
          })
        }
      } else if (def[k] instanceof Array) {
        defaults.push({
          path: thisPath,
          v: []
        })
        if (_.get(def, 'k.0') && Object.keys(def[k][0]).length > 0) {
          // k: [{a: String}]
          getDefaults(thisPath + '.0', def[k][0])
        }
      } else if (typeof def[k] !== 'function') {
        defaults.push({
          path: thisPath,
          v: def[k]
        })
        delete def[k]
      } else {
        console.log('what do do with k', k)
      }
    })
  }

  getDefaults(null, def)
  console.log('defaults for', name, defaults)
  console.log('def for', name, def)

  schema.type(name, def)

  function TypeFactory () {
    return function (obj) {
    console.log('factory for', name, 'def', def, 'obj', obj)

    // add in the default functions
    defaults.map(f => {
      if (f.fn && !f.fn.__type) {
        _.set(obj, f.path, f.fn.bind(obj))
      } else if (typeof f.v !== 'undefined') {
        var specifiedValue = _.get(obj, f.path)
        if (typeof specifiedValue === 'undefined') {
          _.set(obj, f.path, f.v)
        } else if (typeof specifiedValue !== typeof f.v) {
          throw new Error('IncompatibleTypeError: ' + specifiedValue + ' expected to be a ' + f.v.constructor.name)
        }
      }
    })

    check(obj)

    // add the type definition
    if (!obj.__type) {
    Object.defineProperty(obj, '__type', {
      get: function () {
        return name
      }
    })
    }
    return obj
  }
  }

  Object.defineProperty(factory, '__type', {
    get: function () {
      return name
    }
  })

  function check (obj) {
    schema.assert(obj).is(schema[name])
  }

  return new TypeFactory
}

function isDefinedFunction (f) {
  return f instanceof Function 
    && [String, Boolean, Number, Date, Function].indexOf(f) < 0
    && !(f instanceof Type)
    && !(f instanceof TypeFactory)
    && !(f.__type)
}

function defineProp(o, prop, v) {
  Object.defineProperty(o, prop, {
    get: function () {
      return v
    }
  })
}

module.exports = Type
